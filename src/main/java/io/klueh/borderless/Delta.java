package io.klueh.borderless;

import lombok.Data;

@Data
public class Delta {
    private Double x;
    private Double y;
}