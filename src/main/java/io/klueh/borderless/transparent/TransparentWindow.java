package io.klueh.borderless.transparent;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TransparentWindow extends StackPane {

    private final Stage window = new Stage();

    public TransparentWindow() {
        setBackground(new Background(new BackgroundFill(new Color(0.13, 0.13, 0.13, 0.2), CornerRadii.EMPTY,
                Insets.EMPTY)));
        setBorder(new Border(new BorderStroke(new Color(1.0, 1.0, 1.0, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
                BorderStroke.THIN)));

        window.initStyle(StageStyle.TRANSPARENT);
        window.initModality(Modality.NONE);
        window.setScene(new Scene(this, Color.TRANSPARENT));
    }

    public Stage getWindow() {
        return window;
    }

    public void close() {
        window.close();
    }

    public void show() {
        if (!window.isShowing()) {
            window.show();
        } else {
            window.requestFocus();
        }
    }
}