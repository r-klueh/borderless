package io.klueh.borderless.resizecontrol;

import io.klueh.borderless.BorderlessController;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class ResizeControlPane extends Pane {

    private final BorderlessController controller;

    public ResizeControlPane(final BorderlessController controller, final Direction direction) {
        this.controller = controller;

        setCursor(direction.getCursor());
        setBorder(new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
                BorderStroke.MEDIUM)));
        disableProperty().bind(controller.resizableProperty().not());
        AnchorPane.setLeftAnchor(this, direction.getLeftAnchor());
        AnchorPane.setTopAnchor(this, direction.getTopAnchor());
        AnchorPane.setRightAnchor(this, direction.getRightAnchor());
        AnchorPane.setBottomAnchor(this, direction.getBottomAnchor());
        setResizeControl(direction.getDirection());
    }

    private void setResizeControl(final String direction) {
        final Stage stage = controller.getStage();
        setOnDragDetected(event -> {
            controller.setPreviousSize(new Dimension2D(stage.getWidth(), stage.getHeight()));
            controller.setPreviousPosition(new Point2D(stage.getX(), stage.getY()));
        });

        setOnMouseDragged(new DragListener(stage, direction));

        setOnMousePressed(event -> {
            if ((event.isPrimaryButtonDown()) && (!controller.isSnapped())) {
                controller.setPreviousSize(new Dimension2D(controller.getPreviousSize().getWidth(), stage.getHeight()));
                controller.setPreviousPosition(new Point2D(controller.getPreviousPosition().getX(), stage.getY()));
            }
        });

        // Aero Snap Resize.
        setOnMouseReleased(event -> {
            if ((MouseButton.PRIMARY.equals(event.getButton())) && (!controller.isSnapped())) {
                final Rectangle2D screen =
                        Screen.getScreensForRectangle(event.getScreenX(), event.getScreenY(), 1, 1).get(0).getVisualBounds();

                if ((stage.getY() <= screen.getMinY()) && (direction.startsWith("top"))
                        || (event.getScreenY() >= screen.getMaxY()) && (direction.startsWith("bottom"))) {
                    stage.setHeight(screen.getHeight());
                    stage.setY(screen.getMinY());
                    controller.setSnapped(true);
                }
            }
        });

        // Aero Snap resize on double click.
        setOnMouseClicked(event -> {
            if (controller.isDoubleClick(event) && ("top".equals(direction) || "bottom".equals(direction))) {
                final Rectangle2D screen = Screen.getScreensForRectangle(stage.getX(), stage.getY(),
                        stage.getWidth() / 2,
                        stage.getHeight() / 2).get(0).getVisualBounds();

                if (controller.isSnapped()) {
                    stage.setHeight(controller.getPreviousSize().getHeight());
                    stage.setY(controller.getPreviousPosition().getY());
                    controller.setSnapped(false);
                } else {
                    controller.setPreviousSize(new Dimension2D(controller.getPreviousSize().getWidth(),
                            stage.getHeight()));
                    controller.setPreviousPosition(new Point2D(controller.getPreviousPosition().getX(), stage.getY()));
                    stage.setHeight(screen.getHeight());
                    stage.setY(screen.getMinY());
                    controller.setSnapped(true);
                }
            }
        });
    }
}
