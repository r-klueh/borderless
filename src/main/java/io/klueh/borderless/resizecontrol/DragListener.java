package io.klueh.borderless.resizecontrol;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class DragListener implements EventHandler<MouseEvent> {

    private final Stage stage;
    private final String direction;

    @Override
    public void handle(final MouseEvent event) {
        if (event.isPrimaryButtonDown()) {
            resizeHorizontal(event);
            resizeVertical(event);
        }
    }

    private void resizeVertical(final MouseEvent event) {
        double height = stage.getHeight();

        if (isNegativeDirection(Orientation.VERTICAL)) {
            final double comingHeight = height - event.getScreenY() + stage.getY();

            if (isValidValue(comingHeight, stage.getMinHeight())) {
                stage.setHeight(comingHeight);
                stage.setY(event.getScreenY());
            }
        } else if (isPositiveDirection(Orientation.VERTICAL)) {
            double comingHeight = event.getSceneY();

            if (isValidValue(comingHeight, stage.getMinHeight())) {
                stage.setHeight(comingHeight);
            }
        }
    }

    private void resizeHorizontal(final MouseEvent event) {
        double width = stage.getWidth();

        if (isNegativeDirection(Orientation.HORIZONTAL)) {
            double comingWidth = width - event.getScreenX() + stage.getX();

            if (isValidValue(comingWidth, stage.getMinWidth())) {
                stage.setWidth(comingWidth);
                stage.setX(event.getScreenX());
            }

        } else if (isPositiveDirection(Orientation.HORIZONTAL)) {
            double comingWidth = event.getSceneX();

            if (isValidValue(comingWidth, stage.getMinWidth())) {
                stage.setWidth(comingWidth);
            }
        }
    }

    private boolean isNegativeDirection(final Orientation orientation) {
        switch (orientation) {
            case HORIZONTAL -> {
                return direction.endsWith("left");
            }
            case VERTICAL -> {
                return direction.startsWith("top");
            }
        }
        throw new IllegalArgumentException("Unknown orientation");
    }

    private boolean isPositiveDirection(final Orientation orientation) {
        switch (orientation) {
            case HORIZONTAL -> {
                return direction.endsWith("right");
            }
            case VERTICAL -> {
                return direction.startsWith("bottom");
            }
        }
        throw new IllegalArgumentException("Unknown orientation");
    }

    private boolean isValidValue(final double newValue, final double minValue) {
        return newValue > 0 && newValue >= minValue;
    }

    private enum Orientation {
        HORIZONTAL, VERTICAL
    }
}
