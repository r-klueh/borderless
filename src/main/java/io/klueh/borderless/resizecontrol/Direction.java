package io.klueh.borderless.resizecontrol;

import javafx.scene.Cursor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Direction {
    TOP_LEFT(Cursor.NW_RESIZE, 0.0, 0.0, null, null, "top-left"),
    TOP_RIGHT(Cursor.NE_RESIZE, null, 0.0, 0.0, null, "top-right"),
    BOTTOM_RIGHT(Cursor.SE_RESIZE, null, null, 0.0, 0.0, "bottom-right"),
    BOTTOM_LEFT(Cursor.SW_RESIZE, 0.0, null, null, 0.0, "bottom-left"),
    LEFT(Cursor.W_RESIZE, 0.0, 10.0, null, 10.0, "left"),
    TOP(Cursor.N_RESIZE, 10.0, 0.0, 10.0, null, "top"),
    RIGHT(Cursor.E_RESIZE, null, 10.0, 0.0, 10.0, "right"),
    BOTTOM(Cursor.S_RESIZE, 10.0, null, 10.0, 0.0, "bottom");


    private final Cursor cursor;
    private final Double leftAnchor;
    private final Double topAnchor;
    private final Double rightAnchor;
    private final Double bottomAnchor;
    private final String direction;
}
