package io.klueh.borderless;

import io.klueh.borderless.movecontrol.DragListener;
import io.klueh.borderless.movecontrol.ReleaseListener;
import io.klueh.borderless.resizecontrol.Direction;
import io.klueh.borderless.resizecontrol.ResizeControlPane;
import io.klueh.borderless.transparent.TransparentWindow;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class BorderlessController {

    private final SimpleBooleanProperty maximized = new SimpleBooleanProperty(false);
    private final SimpleBooleanProperty resizable = new SimpleBooleanProperty(true);
    private final SimpleBooleanProperty snap = new SimpleBooleanProperty(true);
    @Getter
    private final Stage stage;
    private final TransparentWindow transparentWindow = new TransparentWindow();

    // ---

    @Getter
    @Setter
    private Dimension2D previousSize;
    @Getter
    @Setter
    private boolean snapped;
    @Getter
    @Setter
    private Point2D previousPosition;

    public BorderlessController(final Stage stage) {
        this.stage = stage;
        transparentWindow.getWindow().initOwner(stage);
        previousSize = new Dimension2D(stage.getWidth(), stage.getHeight());
        previousPosition = new Point2D(stage.getX(), stage.getY());
        maximizedProperty().addListener((observable, oldValue, newValue) -> stage.setMaximized(newValue));
    }

    List<Parent> getResizeControlPanes() {
        final Pane topLeftPane = new ResizeControlPane(this, Direction.TOP_LEFT);
        final Pane topRightPane = new ResizeControlPane(this, Direction.TOP_RIGHT);
        final Pane bottomRightPane = new ResizeControlPane(this, Direction.BOTTOM_RIGHT);
        final Pane bottomLeftPane = new ResizeControlPane(this, Direction.BOTTOM_LEFT);
        final Pane leftPane = new ResizeControlPane(this, Direction.LEFT);
        final Pane topPane = new ResizeControlPane(this, Direction.TOP);
        final Pane rightPane = new ResizeControlPane(this, Direction.RIGHT);
        final Pane bottomPane = new ResizeControlPane(this, Direction.BOTTOM);

        return List.of(topLeftPane, topRightPane, bottomLeftPane, bottomRightPane, leftPane, topPane, rightPane,
                bottomPane);
    }

    public BooleanProperty maximizedProperty() {
        return maximized;
    }

    public BooleanProperty resizableProperty() {
        return resizable;
    }

    public BooleanProperty snapProperty() {
        return snap;
    }

    protected void maximize() {
        final Rectangle2D screen;

        try {
            final ObservableList<Screen> screensForRectangle =
                    Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth() / 2,
                            stage.getHeight() / 2);

            if (screensForRectangle.isEmpty())
                screen = Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth(),
                        stage.getHeight()).get(0).getVisualBounds();
            else
                screen = screensForRectangle.get(0).getVisualBounds();

        } catch (final Exception ex) {
            log.error("", ex);
            return;
        }

        if (maximized.get()) {
            stage.setWidth(previousSize.getWidth());
            stage.setHeight(previousSize.getHeight());
            stage.setX(previousPosition.getX());
            stage.setY(previousPosition.getY());
            setMaximized(false);
        } else {
            // Record position and size, and maximize.
            if (!snapped) {
                previousSize = new Dimension2D(stage.getWidth(), stage.getHeight());
                previousPosition = new Point2D(stage.getX(), stage.getY());
            } else {
                resetSizeAndPosition(screen);
            }

            stage.setX(screen.getMinX());
            stage.setY(screen.getMinY());
            stage.setWidth(screen.getWidth());
            stage.setHeight(screen.getHeight());

            setMaximized(true);
        }
    }

    public void resetSizeAndPosition(final Rectangle2D screen) {
        if (!screen.contains(previousPosition.getX(), previousPosition.getY())) {
            previousSize = new Dimension2D(Double.max(previousSize.getWidth(), screen.getWidth() - 20),
                    Double.max(previousSize.getHeight(), screen.getHeight() - 20));

            previousPosition = new Point2D(screen.getMinX() + (screen.getWidth() - previousSize.getWidth()) / 2,
                    screen.getMinY() + (screen.getHeight() - previousSize.getHeight()) / 2);
        }
    }

    protected void setMoveControl(final Node node) {
        final Delta offset = new Delta();

        node.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown()) {

                if (maximized.get() || snapped) {
                    offset.setX(previousSize.getWidth() * (event.getSceneX() / stage.getWidth()));
                    offset.setY(previousSize.getHeight() * (event.getSceneY() / stage.getHeight()));
                } else {
                    offset.setX(event.getSceneX());
                    offset.setY(event.getSceneY());
                    previousSize = new Dimension2D(stage.getWidth(), stage.getHeight());
                    previousPosition = new Point2D(stage.getX(), stage.getY());
                }
            }
        });

        node.setOnMouseDragged(new DragListener(this, stage, offset));

        node.setOnMouseClicked(event -> {
            if (isDoubleClick(event) && snap.get()) {
                maximize();
            }
        });

        node.setOnMouseReleased(new ReleaseListener(this, stage));
    }

    public void snapOff() {
        stage.setWidth(previousSize.getWidth());
        stage.setHeight(previousSize.getHeight());
        snapped = false;
    }

    public boolean isDoubleClick(final MouseEvent event) {
        return (MouseButton.PRIMARY.equals(event.getButton())) && (event.getClickCount() == 2);
    }

    public void setMaximized(final boolean maximized) {
        this.maximized.set(maximized);
        setResizable(!maximized);
    }

    protected void setResizable(final boolean bool) {
        resizable.set(bool);
    }

    protected void setSnapEnabled(final boolean bool) {
        snap.set(bool);
        if (!bool && snapped) {
            snapOff();
        }
    }

    public TransparentWindow getTransparentWindow() {
        return transparentWindow;
    }
}