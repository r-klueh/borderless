package io.klueh.borderless.movecontrol;

import io.klueh.borderless.BorderlessController;
import io.klueh.borderless.transparent.TransparentWindow;
import io.klueh.borderless.Delta;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DragListener implements EventHandler<MouseEvent> {

    private final BorderlessController controller;
    private final Stage stage;
    private final Delta offset;
    private final AeroSnapUtility aeroSnapUtility = new AeroSnapUtility();

    @Override
    public void handle(final MouseEvent event) {

        if (!event.isPrimaryButtonDown()) {
            return;
        }

        moveStage(event);
        aeroSnap(event);
    }

    private void aeroSnap(final MouseEvent event) {
        final TransparentWindow transparentWindow = controller.getTransparentWindow();

        if (controller.maximizedProperty().get()) {
            stage.setWidth(controller.getPreviousSize().getWidth());
            stage.setHeight(controller.getPreviousSize().getHeight());
            controller.setMaximized(false);
        }
        boolean toCloseWindow = false;
        if (!controller.snapProperty().get()) {
            toCloseWindow = true;
        } else {
            final ObservableList<Screen> screens = Screen.getScreensForRectangle(event.getScreenX(),
                    event.getScreenY(), 1, 1);
            if (screens.isEmpty()) {
                return;
            }
            final Rectangle2D screen = screens.get(0).getVisualBounds();

            if (aeroSnapUtility.isAeroSnapTopRight(event, screen)) {
                aeroSnapUtility.aeroSnapTopRight(transparentWindow.getWindow(), screen, 8);
            } else if (aeroSnapUtility.isAeroSnapTopLeft(event, screen)) {
                aeroSnapUtility.aeroSnapTopLeft(transparentWindow.getWindow(), screen, 8);
            } else if (aeroSnapUtility.isAeroSnapBottomRight(event, screen)) {
                aeroSnapUtility.aeroSnapBottomRight(transparentWindow.getWindow(), screen, 8);
            } else if (aeroSnapUtility.isAeroSnapBottomLeft(event, screen)) {
                aeroSnapUtility.aeroSnapBottomLeft(transparentWindow.getWindow(), screen, 8);
            } else if (aeroSnapUtility.isAeroSnapLeft(event, screen)) {
                aeroSnapUtility.aeroSnapLeft(transparentWindow.getWindow(), screen, 8);
            } else if (aeroSnapUtility.isAeroSnapRight(event, screen)) {
                aeroSnapUtility.aeroSnapRight(transparentWindow.getWindow(), screen, 8);
            } else if (aeroSnapUtility.isAeroSnapTop(event, screen) || aeroSnapUtility.isAeroSnapBottom(event,
                    screen)) {
                aeroSnapUtility.aeroSnapTopOrBottom(transparentWindow.getWindow(), screen, 8);
            } else {
                toCloseWindow = true;
            }
        }

        if (toCloseWindow) {
            transparentWindow.close();
        } else {
            transparentWindow.show();
        }
    }

    private void moveStage(final MouseEvent event) {
        stage.setX(event.getScreenX() - offset.getX());

        if (controller.isSnapped()) {
            controller.snapOff();
        } else {
            stage.setY(event.getScreenY() - offset.getY());
        }
    }
}
