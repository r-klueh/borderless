package io.klueh.borderless.movecontrol;

import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class AeroSnapUtility {

    boolean isAeroSnapBottom(final MouseEvent event, final Rectangle2D screen) {
        return event.getScreenY() >= screen.getMaxY() - 10;
    }

    boolean isAeroSnapTop(final MouseEvent event, final Rectangle2D screen) {
        return event.getScreenY() <= screen.getMinY() + 10;
    }

    boolean isAeroSnapRight(final MouseEvent event, final Rectangle2D screen) {
        return event.getScreenX() >= screen.getMaxX() - 10;
    }

    boolean isAeroSnapLeft(final MouseEvent event, final Rectangle2D screen) {
        return event.getScreenX() <= screen.getMinX() + 10;
    }

    boolean isAeroSnapBottomLeft(final MouseEvent event, final Rectangle2D screen) {
        return isAeroSnapBottom(event, screen) && isAeroSnapLeft(event, screen);
    }

    boolean isAeroSnapBottomRight(final MouseEvent event, final Rectangle2D screen) {
        return isAeroSnapBottom(event, screen) && isAeroSnapRight(event, screen);
    }

    boolean isAeroSnapTopLeft(final MouseEvent event, final Rectangle2D screen) {
        return isAeroSnapTop(event, screen) && isAeroSnapLeft(event, screen);
    }

    boolean isAeroSnapTopRight(final MouseEvent event, final Rectangle2D screen) {
        return isAeroSnapTop(event, screen) && isAeroSnapRight(event, screen);
    }

    void aeroSnapRight(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setY(screen.getMinY() + offset);
        stage.setHeight(screen.getHeight() - 2 * offset);

        stage.setWidth(Math.max(screen.getWidth() / 2, stage.getMinWidth()) - offset);
        stage.setX(screen.getMaxX() - stage.getWidth() - offset);
    }

    void aeroSnapLeft(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setY(screen.getMinY() + offset);
        stage.setHeight(screen.getHeight() - 2 * offset);

        stage.setX(screen.getMinX() + offset);
        stage.setWidth(Math.max(screen.getWidth() / 2, stage.getMinWidth()) - offset);
    }

    void aeroSnapTopOrBottom(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setX(screen.getMinX() + offset);
        stage.setY(screen.getMinY() + offset);
        stage.setWidth(screen.getWidth() - 2 * offset);
        stage.setHeight(screen.getHeight() - 2 * offset);
    }

    void aeroSnapBottomLeft(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setX(screen.getMinX() + offset);
        stage.setY(screen.getHeight() / 2 - (screen.getHeight() - screen.getHeight()) + offset);
        stage.setWidth(screen.getWidth() / 2 - offset);
        stage.setHeight(screen.getHeight() / 2 - 2 * offset);
    }

    void aeroSnapBottomRight(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setX(screen.getWidth() / 2 - (screen.getWidth() - screen.getWidth()) - offset);
        stage.setY(screen.getHeight() / 2 - (screen.getHeight() - screen.getHeight()) + offset);
        stage.setWidth(screen.getWidth() / 2 - offset);
        stage.setHeight(screen.getHeight() / 2 - 2 * offset);
    }

    void aeroSnapTopLeft(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setX(screen.getMinX() + offset);
        stage.setY(screen.getMinY() + offset);
        stage.setWidth(screen.getWidth() / 2 - offset);
        stage.setHeight(screen.getHeight() / 2 - offset);
    }

    void aeroSnapTopRight(final Stage stage, final Rectangle2D screen, final int offset) {
        stage.setX(screen.getWidth() / 2);
        stage.setY(screen.getMinY() + offset);
        stage.setWidth(screen.getWidth() / 2 - 2 * offset);
        stage.setHeight(screen.getHeight() / 2 - offset);
    }
}
