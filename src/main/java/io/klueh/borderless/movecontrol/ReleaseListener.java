package io.klueh.borderless.movecontrol;

import io.klueh.borderless.BorderlessController;
import io.klueh.borderless.transparent.TransparentWindow;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class ReleaseListener implements EventHandler<MouseEvent> {


    private final BorderlessController controller;
    private final Stage stage;
    private final AeroSnapUtility aeroSnapUtility = new AeroSnapUtility();

    @Override
    public void handle(final MouseEvent event) {
        final TransparentWindow transparentWindow = controller.getTransparentWindow();

        if (!controller.snapProperty().get() || !MouseButton.PRIMARY.equals(event.getButton())) {
            return;
        }
        final Rectangle2D screen =
                Screen.getScreensForRectangle(event.getScreenX(), event.getScreenY(), 1, 1).get(0).getVisualBounds();

        boolean snapping = true;
        if (aeroSnapUtility.isAeroSnapTopRight(event, screen)) {
            aeroSnapUtility.aeroSnapTopRight(stage, screen, 0);
        } else if (aeroSnapUtility.isAeroSnapTopLeft(event, screen)) {
            aeroSnapUtility.aeroSnapTopLeft(stage, screen, 0);
        } else if (aeroSnapUtility.isAeroSnapBottomRight(event, screen)) {
            aeroSnapUtility.aeroSnapBottomRight(stage, screen, 0);
        } else if (aeroSnapUtility.isAeroSnapBottomLeft(event, screen)) {
            aeroSnapUtility.aeroSnapBottomLeft(stage, screen, 0);
        } else if (aeroSnapUtility.isAeroSnapLeft(event, screen)) {
            aeroSnapUtility.aeroSnapLeft(stage, screen, 0);
        } else if (aeroSnapUtility.isAeroSnapRight(event, screen)) {
            aeroSnapUtility.aeroSnapRight(stage, screen, 0);
        } else if (aeroSnapUtility.isAeroSnapTop(event, screen) || aeroSnapUtility.isAeroSnapBottom(event,
                screen)) {
            controller.resetSizeAndPosition(screen);
            aeroSnapUtility.aeroSnapTopOrBottom(stage, screen, 0);
            snapping = false;
        }

        if (snapping) {
            controller.setSnapped(true);
        } else {
            controller.setMaximized(true);
        }

        transparentWindow.close();
    }
}
