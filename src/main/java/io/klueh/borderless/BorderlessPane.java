package io.klueh.borderless;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;

public class BorderlessPane extends AnchorPane {

    private static final String DEFAULT_STYLE_CLASS = "borderless-pane";

    @Getter
    private final BorderlessController controller;
    private final Stage stage;

    public BorderlessPane(final Stage stage, final Parent content) {
        this.stage = stage;
        controller = new BorderlessController(stage);
        minHeight(Double.NEGATIVE_INFINITY);
        minWidth(Double.NEGATIVE_INFINITY);
        maxHeight(Double.NEGATIVE_INFINITY);
        maxWidth(Double.NEGATIVE_INFINITY);

        getChildren().add(content);
        getChildren().addAll(controller.getResizeControlPanes());

        AnchorPane.setLeftAnchor(content, 0.0D);
        AnchorPane.setTopAnchor(content, 0.0D);
        AnchorPane.setRightAnchor(content, 0.0D);
        AnchorPane.setBottomAnchor(content, 0.0D);

        getStyleClass().setAll(DEFAULT_STYLE_CLASS);
    }

    public void setContent(final Node content) {
        getChildren().set(0, content);

        AnchorPane.setLeftAnchor(content, 0.0D);
        AnchorPane.setTopAnchor(content, 0.0D);
        AnchorPane.setRightAnchor(content, 0.0D);
        AnchorPane.setBottomAnchor(content, 0.0D);

        stage.sizeToScene();
    }

    @Override
    public String getUserAgentStylesheet() {
        return BorderlessPane.class.getResource("borderless-pane.css").toExternalForm();
    }
}