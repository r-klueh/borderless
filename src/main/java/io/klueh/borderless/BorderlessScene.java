package io.klueh.borderless;

import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BorderlessScene extends Scene {

    private final BorderlessController controller;

    public BorderlessScene(final Stage stage, final StageStyle stageStyle, final Parent sceneRoot) {
        super(new BorderlessPane(stage, sceneRoot));

        this.controller = ((BorderlessPane) getRoot()).getController();

        stage.initStyle(stageStyle);
        if (stageStyle == StageStyle.UTILITY) {
            setSnapEnabled(false);
            setResizable(false);
        }
    }

    public void setMoveControl(final Node node) {
        this.controller.setMoveControl(node);
    }

    public BooleanProperty maximizedProperty() {
        return controller.maximizedProperty();
    }

    public BooleanProperty resizableProperty() {
        return controller.resizableProperty();
    }

    public BooleanProperty snapProperty() {
        return controller.snapProperty();
    }

    public boolean isResizable() {
        return controller.resizableProperty().get();
    }

    public void setResizable(boolean bool) {
        controller.setResizable(bool);
    }

    public boolean isSnapEnabled() {
        return controller.snapProperty().get();
    }

    public void setSnapEnabled(boolean bool) {
        controller.setSnapEnabled(bool);
    }
}